import { action, Action, createStore, createTypedHooks } from 'easy-peasy';
import { ArticleItem } from '../graphql/queries/articles';

export type StoreModel = {
  article: {
    items: ArticleItem | undefined;
    add: Action<StoreModel, ArticleItem>;
  };
};

export const store = createStore<StoreModel>({
  article: {
    items: undefined,
    add: action((state, payload) => {
      return { ...state, items: payload };
    }),
  },
});

const typedHooks = createTypedHooks<StoreModel>();
export const useStoreActions = typedHooks.useStoreActions;
export const useStoreDispatch = typedHooks.useStoreDispatch;
export const useStoreState = typedHooks.useStoreState;
