import React from 'react';
import { BottomLine, Wrapper, WrapperTitle } from './Header.styled';

const Header = () => {
  return (
    <Wrapper>
      <WrapperTitle>Marble</WrapperTitle>
      <BottomLine />
    </Wrapper>
  );
};

export default Header;
