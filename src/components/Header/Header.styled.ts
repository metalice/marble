import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  height: 80px;
  background: #ffffff;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

export const BottomLine = styled.div`
  border: 1px solid #e2eef1;
`;

export const WrapperTitle = styled.div`
  margin-top: 32px;
  color: #2e4056;
  font-weight: bold;
  font-size: 26px;
`;
