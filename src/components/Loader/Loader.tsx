import React from 'react';
import svg from '../../assets/svg/loader.svg';
import { Wrapper } from './Loader.styled';

const Loader = () => {
  return (
    <Wrapper>
      <img src={svg} />
    </Wrapper>
  );
};

export default Loader;
