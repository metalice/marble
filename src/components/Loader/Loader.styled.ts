import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  height: 200px;
  justify-content: center;
  align-items: center;
`;
