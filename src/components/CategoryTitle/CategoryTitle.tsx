import React from 'react';
import { Wrapper } from './CategoryTitle.style';

type CategoryTitleProps = {
  title: string;
};
const CategoryTitle: React.FC<CategoryTitleProps> = ({
  title,
}: CategoryTitleProps) => {
  return <Wrapper>{title}</Wrapper>;
};

export default CategoryTitle;
