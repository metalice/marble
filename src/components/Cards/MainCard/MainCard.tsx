import { format } from 'date-fns';
import React from 'react';
import { Link } from 'react-router-dom';
import { Card } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { ArticleItem } from '../../../graphql/queries/articles';
import { useStoreActions } from '../../../store/index';
import { Wrapper, WrapperDate, WrapperTitle } from './MainCard.styled';

type MainCardProps = {
  item: ArticleItem;
  mainCard?: boolean;
};

const MainCard: React.FC<MainCardProps> = ({ item }: MainCardProps) => {
  const { content, slug, id } = item || {};
  const addArticle = useStoreActions((actions) => actions?.article?.add);
  const publishDate =
    item?.created_at && format(new Date(item.created_at), 'MMM dd, yyyy');

  return (
    <Wrapper>
      <Card sx={{ boxShadow: 'none', height: 448 }}>
        <Link to={`/article/${slug}--${id}`} onClick={() => addArticle(item)}>
          <CardMedia
            component="img"
            image={content?.headPicture?.filename}
            sx={{
              border: 'none',
              margin: '24px 24px 0 24px',
              width: 592,
              height: 232,
              borderRadius: '12px 12px 0 0',
            }}
          />
          <CardContent sx={{ padding: '16px 16px 0 24px' }}>
            <WrapperDate>{publishDate}</WrapperDate>
            <WrapperTitle>{content?.heading}</WrapperTitle>
          </CardContent>
          <CardActions>
            <Button
              variant="outlined"
              size="small"
              sx={{ margin: '32px 0 24px 18px' }}
            >
              {content?.categories?.name}
            </Button>
          </CardActions>
        </Link>
      </Card>
    </Wrapper>
  );
};

export default MainCard;
