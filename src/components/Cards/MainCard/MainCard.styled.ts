import styled from 'styled-components';

export const Wrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 8px 24px #dce2ff;
  border-radius: 12px;
`;

export const WrapperDate = styled.div`
  font-family: Assistant;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  color: #2e4056;
`;

export const WrapperTitle = styled.div`
  font-family: Assistant;
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 32px;
  color: #2e4056;
`;
