import styled from 'styled-components';

export const Wrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 8px 24px #dce2ff;
  border-radius: 12px;
  margin-bottom: 32px;
`;

export const WrapperCardTitle = styled.div`
  font-family: Assistant;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  color: #2e4056;
`;
