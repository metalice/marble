import React from 'react';
import { Link } from 'react-router-dom';
import { Box, Card } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { ArticleItem } from '../../../graphql/queries/articles';
import { useStoreActions } from '../../../store/index';
import { Wrapper, WrapperCardTitle } from './SmallCard.styled';

type SmallCardProps = {
  item: ArticleItem;
};

const SmallCard: React.FC<SmallCardProps> = ({ item }: SmallCardProps) => {
  const { content, slug, id } = item;
  const addArticle = useStoreActions((actions) => actions?.article?.add);

  return (
    <Wrapper>
      <Link to={`/article/${slug}--${id}`} onClick={() => addArticle(item)}>
        <Card sx={{ display: 'flex', height: 128 }}>
          <CardMedia
            component="img"
            sx={{ width: '117px', height: '151px' }}
            image={content?.headPicture?.filename}
          />
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <CardContent sx={{ paddingBottom: 0 }}>
              <WrapperCardTitle>{content?.heading}</WrapperCardTitle>
            </CardContent>
            <CardActions
              sx={{ height: '24px', padding: 0, margin: '16px 0 24px 16px' }}
            >
              <Button
                variant="outlined"
                sx={{
                  height: '24px',
                  fontFamily: 'Assistant',
                  fontSize: '12px',
                  fontWeight: 600,
                  color: '#2E4056',
                }}
              >
                {content?.categories?.name}
              </Button>
            </CardActions>
          </Box>
        </Card>
      </Link>
    </Wrapper>
  );
};

export default SmallCard;
