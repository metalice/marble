import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 352px;
  height: 249px;
  margin-top: 40px;
  margin-bottom: 88px;
  background: #ffffff;
  box-shadow: 0px 8px 24px #dce2ff;
  border-radius: 8px;
`;

export const WrapperCardTitle = styled.div`
  font-family: Assistant;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  color: #2e4056;
`;
