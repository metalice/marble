import React from 'react';
import { Link } from 'react-router-dom';
import { Card } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { ArticleItem } from '../../../graphql/queries/articles';
import { useStoreActions } from '../../../store/index';
import { Wrapper, WrapperCardTitle } from './CategoryCard.styled';

type CategoryCardProps = {
  item: ArticleItem;
};

const CategoryCard: React.FC<CategoryCardProps> = ({
  item,
}: CategoryCardProps) => {
  const { content, slug, id } = item || {};
  const addArticle = useStoreActions((actions) => actions?.article?.add);

  return (
    <Wrapper>
      <Card sx={{ boxShadow: 'none' }}>
        <Link to={`/article/${slug}--${id}`} onClick={() => addArticle(item)}>
          <CardMedia
            component="img"
            image={content?.headPicture?.filename}
            sx={{ width: 352, height: 120 }}
          />
          <CardContent>
            <WrapperCardTitle>{content?.heading}</WrapperCardTitle>
          </CardContent>
          <CardActions>
            <Button
              variant="outlined"
              sx={{
                height: 24,
                fontSize: 12,
                lineHeight: '16px',
                color: '#2E4056',
              }}
            >
              {content?.categories?.name}
            </Button>
          </CardActions>
        </Link>
      </Card>
    </Wrapper>
  );
};

export default CategoryCard;
