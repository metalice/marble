import React from 'react';
import { Wrapper } from './LoadingError.styled';

const LoadingError = () => {
  return <Wrapper>Oh no, something went wrong! Please try to refresh.</Wrapper>;
};

export default LoadingError;
