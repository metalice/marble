import React from 'react';
import { useQuery } from '@apollo/client';
import {
  ARTICLES_QUERY,
  ARTICLES_QUERY_STRUCTURE,
} from '../../graphql/queries/articles';
import Loader from '../Loader/Loader';
import LoadingError from '../LoadingError/LoadingError';
import { Wrapper } from './Home.styled';
import HomeContent from './HomeContent/HomeContent';
import BlogTitles from './HomeTitles/HomeTitles';

const Home: React.FC = () => {
  const { data, loading, error } =
    useQuery<ARTICLES_QUERY_STRUCTURE>(ARTICLES_QUERY);

  return (
    <Wrapper>
      <BlogTitles />
      {error && <LoadingError />}
      {loading ? (
        <Loader />
      ) : (
        <HomeContent data={data as ARTICLES_QUERY_STRUCTURE} />
      )}
    </Wrapper>
  );
};

export default Home;
