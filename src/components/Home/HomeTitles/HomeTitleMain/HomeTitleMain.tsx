import React from 'react';
import { Wrapper } from './HomeTitleMain.styled';

const BlogTitleMain = () => {
  return <Wrapper>Marbel Blog</Wrapper>;
};

export default BlogTitleMain;
