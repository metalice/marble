import styled from 'styled-components';

export const Wrapper = styled.div`
  font-family: Playfair Display;
  font-style: normal;
  font-weight: bold;
  font-size: 80px;
  line-height: 90px;
  margin-top: 120px;
  color: #2e4056;
`;
