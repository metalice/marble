import React from 'react';
import BlogTitleMain from './HomeTitleMain/HomeTitleMain';
import BlogTitleSecondary from './HomeTitleSecondary/HomeTitleSecondary';

const BlogTitles = () => {
  return (
    <>
      <BlogTitleMain />
      <BlogTitleSecondary />
    </>
  );
};

export default BlogTitles;
