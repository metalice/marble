import { sample, sampleSize } from 'lodash';
import React from 'react';
import { ArticleItem } from '../../../../graphql/queries/articles';
import MainCard from '../../../Cards/MainCard/MainCard';
import SmallCard from '../../../Cards/SmallCard/SmallCard';
import {
  Wrapper,
  WrapperArticleCard,
  WrapperArticleCardsSmall,
} from './HomeUpperPart.styled';

type HomeUpperPartProps = {
  articleItems: ArticleItem[];
};

const HomeUpperPart: React.FC<HomeUpperPartProps> = ({
  articleItems,
}: HomeUpperPartProps) => {
  const mainCardItem = sample(articleItems) as ArticleItem;
  const smallCards = sampleSize(articleItems, 3) as ArticleItem[];

  return (
    <Wrapper>
      <WrapperArticleCard>
        <MainCard item={mainCardItem} />
      </WrapperArticleCard>
      <WrapperArticleCardsSmall>
        {smallCards.map((sm, index) => (
          <SmallCard item={sm} key={index} />
        ))}
      </WrapperArticleCardsSmall>
    </Wrapper>
  );
};

export default HomeUpperPart;
