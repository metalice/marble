import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  margin-top: 160px;
  margin-bottom: 160px;
`;

export const WrapperArticleCard = styled.div`
  width: 640px;
`;

export const WrapperArticleCardsSmall = styled.div`
  display: flex;
  width: 448px;
  flex-direction: column;
  height: 448px;
`;
