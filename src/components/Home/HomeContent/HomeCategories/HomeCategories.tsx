import React from 'react';
import { BottomLine } from 'src/components/Header/Header.styled';
import { ArticleItem } from '../../../../graphql/queries/articles';
import CategoryCard from '../../../Cards/CategoryCard/CategoryCard';
import CategoryTitle from '../../../CategoryTitle/CategoryTitle';
import { WrapperCategoryCards } from './HomeCategories.styled';

type HomeCategoriesProps = {
  articleItems: ArticleItem[];
};

export const HomeCategories: React.FC<HomeCategoriesProps> = ({
  articleItems,
}: HomeCategoriesProps) => {
  const categories = Object.entries(
    articleItems?.reduce((acc, item) => {
      const category = item?.content?.categories?.name;
      acc[category] = [...(acc[category] || []), item];
      return acc;
    }, {} as { [key: string]: ArticleItem[] }) || [],
  );

  return (
    <div>
      {categories.map((category, index) => {
        return (
          <div key={category[0]}>
            <CategoryTitle title={category[0]} />
            <WrapperCategoryCards>
              {category?.[1].map((item) => (
                <CategoryCard item={item} key={item?.slug} />
              ))}
            </WrapperCategoryCards>
            {index % 2 === 0 && <BottomLine />}
          </div>
        );
      })}
    </div>
  );
};
