import styled from 'styled-components';

export const WrapperCategoryCards = styled.div`
  display: flex;
  justify-content: space-between;
`;
