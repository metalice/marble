import React from 'react';
import { BottomLine } from 'src/components/Header/Header.styled';
import { ARTICLES_QUERY_STRUCTURE } from 'src/graphql/queries/articles';
import { HomeCategories } from './HomeCategories/HomeCategories';
import HomeUpperPart from './HomeUpperPart/HomeUpperPart';

type HomeContentProps = {
  data: ARTICLES_QUERY_STRUCTURE;
};

const HomeContent: React.FC<HomeContentProps> = ({
  data,
}: HomeContentProps) => {
  const articleItems = data?.ArticleItems?.items;

  return (
    <div>
      <HomeUpperPart articleItems={articleItems} />
      <BottomLine />
      <HomeCategories articleItems={articleItems} />
    </div>
  );
};

export default HomeContent;
