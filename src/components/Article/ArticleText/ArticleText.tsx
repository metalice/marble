import React from 'react';
import { Wrapper } from './ArticleText.styled';

type ArticleTextProps = { text: string };
const ArticleText: React.FC<ArticleTextProps> = ({
  text,
}: ArticleTextProps) => {
  return <Wrapper>{text}</Wrapper>;
};

export default ArticleText;
