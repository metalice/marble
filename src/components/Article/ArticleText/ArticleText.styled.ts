import styled from 'styled-components';

export const Wrapper = styled.div`
  font-family: Assistant;
  font-style: normal;
  font-weight: normal;
  font-size: 21px;
  line-height: 32px;
  margin-bottom: 48px;
  color: #2e4056;
`;
