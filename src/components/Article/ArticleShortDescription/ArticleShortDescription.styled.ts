import styled from 'styled-components';

export const Wrapper = styled.div`
  font-family: Assistant;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  color: #2e4056;
  margin-bottom: 48px;
`;
