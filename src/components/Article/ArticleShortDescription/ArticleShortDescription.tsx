import React from 'react';
import { Wrapper } from './ArticleShortDescription.styled';

type ArticleShortDescriptionProps = { description: string };

const ArticleShortDescription = ({
  description,
}: ArticleShortDescriptionProps) => {
  return <Wrapper>{description}</Wrapper>;
};

export default ArticleShortDescription;
