import styled from 'styled-components';

export const Wrapper = styled.div`
  margin: 0 192px;
`;

export const WrapperImage = styled.img`
  width: 100%;
  height: auto;
  border-radius: 12px;
  margin-bottom: 86px;
`;
