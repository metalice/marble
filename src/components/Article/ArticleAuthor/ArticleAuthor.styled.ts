import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  margin-bottom: 50px;
`;

export const WrapperImage = styled.img`
  height: 48px;
  width: 48px;
  border-radius: 50px;
  margin-right: 16px;
`;

export const WrapperName = styled.div`
  font-family: Assistant;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  color: #2e4056;
`;

export const WrapperDate = styled.div`
  font-family: Assistant;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  color: #2e4056;
`;
