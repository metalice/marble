import { format } from 'date-fns';
import React from 'react';
import { ArticleAuthorItem } from '../../../graphql/queries/articles';
import {
  Wrapper,
  WrapperDate,
  WrapperImage,
  WrapperName,
} from './ArticleAuthor.styled';

type ArticleAuthorProps = {
  author: ArticleAuthorItem;
  createdAt: string;
};

const ArticleAuthor: React.FC<ArticleAuthorProps> = ({
  author,
  createdAt,
}: ArticleAuthorProps) => {
  const { content } = author || {};
  const publishDate = createdAt && format(new Date(createdAt), 'MMM dd, yyyy');

  return (
    <Wrapper>
      <WrapperImage src={content?.picture?.filename} />
      <div>
        <WrapperName>{content?.name}</WrapperName>
        <WrapperDate>{publishDate}</WrapperDate>
      </div>
    </Wrapper>
  );
};

export default ArticleAuthor;
