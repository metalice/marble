import { useStoreState } from 'easy-peasy';
import React from 'react';
import { useParams } from 'react-router-dom';
import { StoreModel } from 'src/store';
import { useQuery } from '@apollo/client';
import {
  ARTICLE_QUERY,
  ARTICLE_QUERY_STRUCTURE,
  ArticleItem,
} from '../../graphql/queries/articles';
import Loader from '../Loader/Loader';
import LoadingError from '../LoadingError/LoadingError';
import { Wrapper, WrapperImage } from './Article.styled';
import ArticleAuthor from './ArticleAuthor/ArticleAuthor';
import ArticleShortDescription from './ArticleShortDescription/ArticleShortDescription';
import ArticleSubTitle from './ArticleSubTitle/ArticleSubTitle';
import ArticleText from './ArticleText/ArticleText';
import ArticleTitle from './ArticleTitle/ArticleTitle';

const Article: React.FC = () => {
  const { articleId } = useParams<{ [key: string]: string }>();

  const article = useStoreState(
    (state: StoreModel) => state?.article?.items,
  ) as ArticleItem;

  const { data, loading, error } = useQuery<ARTICLE_QUERY_STRUCTURE>(
    ARTICLE_QUERY,
    {
      variables: { id: articleId },
      skip: !!article,
    },
  );

  const { content, created_at } = article || data?.ArticleItem || {};

  const items =
    content?.articleItems?.reduce(
      (acc, articleItem, index, originalArticleItems) => {
        if (index % 2 === 0) {
          acc[articleItem?.text] = originalArticleItems[index + 1]?.text;
        }
        return acc;
      },
      {} as { [key: string]: string },
    ) || {};

  return (
    <Wrapper>
      {error && <LoadingError />}
      {loading ? (
        <Loader />
      ) : (
        <>
          <ArticleTitle title={content?.heading} />
          <ArticleShortDescription description={content?.description} />
          <ArticleAuthor author={content?.Author} createdAt={created_at} />
          <WrapperImage src={content?.headPicture?.filename} />
          {Object.entries(items)?.map(([subTitle, text]) => {
            return (
              <div key={subTitle}>
                <ArticleSubTitle title={subTitle} />
                <ArticleText text={text} />
              </div>
            );
          })}
        </>
      )}
    </Wrapper>
  );
};

export default Article;
