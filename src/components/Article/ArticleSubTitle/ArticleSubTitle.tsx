import React from 'react';
import { Wrapper } from './ArticleSubTitle.styled';

type ArticleSubTitleProps = {
  title: string;
};
const ArticleSubTitle = ({ title }: ArticleSubTitleProps) => {
  return <Wrapper>{title}</Wrapper>;
};

export default ArticleSubTitle;
