import styled from 'styled-components';

export const Wrapper = styled.div`
  font-family: Assistant;
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  color: #2e4056;
  margin-bottom: 24px;
`;
