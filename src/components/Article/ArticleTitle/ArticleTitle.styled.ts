import styled from 'styled-components';

export const Wrapper = styled.div`
  font-family: Playfair Display;
  font-style: normal;
  font-weight: bold;
  font-size: 32px;
  line-height: 40px;
  color: #2e4056;
  margin-top: 88px;
  margin-bottom: 40px;
`;
