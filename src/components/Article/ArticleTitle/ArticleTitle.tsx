import React from 'react';
import { Wrapper } from './ArticleTitle.styled';

type ArticleTitleProps = {
  title: string;
};
const ArticleTitle: React.FC<ArticleTitleProps> = ({
  title,
}: ArticleTitleProps) => {
  return <Wrapper>{title}</Wrapper>;
};

export default ArticleTitle;
