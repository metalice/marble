import { StoreProvider } from 'easy-peasy';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Article from './components/Article/Article';
import Header from './components/Header/Header';
import Home from './components/Home/Home';
import { store } from './store';
import '@fontsource/assistant';
import '@fontsource/playfair-display';
import 'normalize.css';

const App = () => {
  return (
    <div className="App">
      <StoreProvider store={store}>
        <Router>
          <div>
            <Header />
            <Switch>
              <Route path="/article/:slug--:articleId">
                <Article />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </div>
        </Router>
      </StoreProvider>
    </div>
  );
};

export default App;
