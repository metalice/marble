import { gql } from '@apollo/client';

export const ARTICLES_QUERY = gql`
  query {
    ArticleItems(by_slugs: "articles/*") {
      items {
        name
        slug
        id
        created_at
        content {
          BreadCrumbs
          Author {
            content
          }
          articleItems
          categories {
            name
          }
          headPicture {
            filename
          }
          heading
        }
      }
    }
  }
`;

export const ARTICLE_QUERY = gql`
  query getArticle($id: ID!) {
    ArticleItem(id: $id) {
      created_at
      content {
        Author {
          content
        }
        description
        articleItems
        headPicture {
          filename
        }
        heading
      }
    }
  }
`;

export type ARTICLES_QUERY_STRUCTURE = {
  ArticleItems: {
    items: ArticleItem[];
  };
};

export type ARTICLE_QUERY_STRUCTURE = {
  ArticleItem: ArticleItem;
};

type ArticleItemContent = {
  component: string;
  text: string;
};

export type ArticleAuthorItem = {
  content: { name: string; picture: { filename: string } };
};

export type ArticleItem = {
  name: string;
  slug: string;
  id: string;
  created_at: string;
  content: {
    BreadCrumbs: string;
    Author: ArticleAuthorItem;
    description: string;
    articleItems: ArticleItemContent[];
    categories: {
      name: string;
    };
    headPicture: {
      filename: string;
    };
    heading: string;
  };
};
